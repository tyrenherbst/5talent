﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using _5Talent.DAL;
using _5Talent.Models;

namespace _5Talent.Controllers
{
    public class listingForRealtorsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: listingForRealtors
        public ActionResult Index(string sMLS,string sCity, string sState, string sZip,string sBed, string sBath, string sSquare)
        {
            var listings = from l in db.listings select l;
            
            if (!String.IsNullOrEmpty(sMLS))
            {
                listings = listings.Where(l => l.MLS.ToUpper().Trim().Contains(sMLS.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sCity))
            {
                listings = listings.Where(l => l.City.ToUpper().Trim().Contains(sCity.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sState))
            {
                listings = listings.Where(l => l.State.ToUpper().Trim().Contains(sState.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sZip))
            {
                listings = listings.Where(l => l.Zipcode.ToUpper().Trim().Contains(sZip.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sBed))
            {
                listings = listings.Where(l => l.Bedrooms.ToUpper().Trim().Contains(sBed.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sBath))
            {
                listings = listings.Where(l => l.Bathrooms.ToUpper().Trim().Contains(sBath.ToUpper().Trim()));
            }
            if (!String.IsNullOrEmpty(sSquare))
            {
                listings = listings.Where(l => l.SquareFeet.ToUpper().Trim().Contains(sSquare.ToUpper().Trim()));
            }

            listings = listings.OrderBy(l => l.MLS);
            return View(listings.ToList());
        }

        // GET: listingForRealtors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            listingForRealtors listingForRealtors = db.listings.Find(id);
            if (listingForRealtors == null)
            {
                return HttpNotFound();
            }
            return View(listingForRealtors);
        }

        // GET: listingForRealtors/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: listingForRealtors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "MLS,Street1,Street2,City,State,Zipcode,Neighborhood,SalesPrice,DateListed,Bedrooms,Photos,Bathrooms,GarageSize,SquareFeet,LotSize,Description")] listingForRealtors listingForRealtors)
        {
            if (ModelState.IsValid)
            {
                db.listings.Add(listingForRealtors);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(listingForRealtors);
        }

        // GET: listingForRealtors/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            listingForRealtors listingForRealtors = db.listings.Find(id);
            if (listingForRealtors == null)
            {
                return HttpNotFound();
            }
            return View(listingForRealtors);
        }

        // POST: listingForRealtors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "MLS,Street1,Street2,City,State,Zipcode,Neighborhood,SalesPrice,DateListed,Bedrooms,Photos,Bathrooms,GarageSize,SquareFeet,LotSize,Description")] listingForRealtors listingForRealtors)
        {
            if (ModelState.IsValid)
            {
                db.Entry(listingForRealtors).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(listingForRealtors);
        }

        // GET: listingForRealtors/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            listingForRealtors listingForRealtors = db.listings.Find(id);
            if (listingForRealtors == null)
            {
                return HttpNotFound();
            }
            return View(listingForRealtors);
        }

        // POST: listingForRealtors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            listingForRealtors listingForRealtors = db.listings.Find(id);
            db.listings.Remove(listingForRealtors);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
