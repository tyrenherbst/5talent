﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_5Talent.Startup))]
namespace _5Talent
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
