﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace _5Talent.Models
{
    public class listingForRealtors
    {
        public int ID { get; set; }
        public string MLS { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Neighborhood { get; set; }
        public string SalesPrice { get; set; }
        public DateTime DateListed { get; set; }
        public string Bedrooms { get; set; }
        public string Photos { get; set; }
        public string Bathrooms { get; set; }
        public string GarageSize { get; set; }
        public string SquareFeet { get; set; }
        public string LotSize { get; set; }
        public string Description { get; set; }
    }
}