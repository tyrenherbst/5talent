﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace _5Talent.Models
{
    public class listingForRealtors
    {
        public int MLS { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int Zipcode { get; set; }
        public string Neighborhood { get; set; }
        public int SalesPrice { get; set; }
        public DateTime DateListed { get; set; }
        public int Bedrooms { get; set; }
        public Image Photos { get; set; }
        public int Bathrooms { get; set; }
        public string GarageSize { get; set; }
        public int SquareFeet { get; set; }
        public string LotSize { get; set; }
        public string Description { get; set; }
    }
}