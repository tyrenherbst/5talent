﻿using _5Talent.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace _5Talent.DAL
{
    public class ModelContext : DbContext
    {
        public ModelContext() : base("ModelContext") { }
        public DbSet<listingForRealtors> listings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}